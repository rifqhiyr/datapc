// console.log('satu');
// setTimeout(()=> { console.log('dua')},100);
// console.log('tiga');

// const posts = [
//     {
//         title: 'post one',
//         body: 'this is post one'
//     },
//     {
//         title: 'post two',
//         body: 'this is post two'
//     }
// ]

// const createPost = (post) => {
//     setTimeout(() => {
//         posts.push(post)
//     }, 2000)
// }

// const getPosts = () => {
//     setTimeout(() => {
//         posts.forEach(x => {
//             console.log(`${x.title} ** ${x.body}`)
//         })
//     }, 1000)
// }


// //membuat post baru
// createPost({
//     title: "post three",
//     body: "this is post three"
// })

// getPosts()

// var promise1 = new Promise(function(resolve, reject) {
//     setTimeout(function(){
//         resolve('foo');
//     }, 300);
// });

// console.log(promise1);

const posts = [
    {
        title: 'post one',
        body: 'this is post one'
    },
    {
        title: 'post two',
        body: 'this is post two'
    }
]

const createPost = (post) => {
    return new Promise((resolve, reject) => {
        setTimeout(()=> {
            posts.push(post)
            const error = false
            if(!error){
                resolve()
            }else{
                reject()
            }
            }, 2000)
            
        })
}

const getPosts = () => {
    setTimeout(() => {
        posts.forEach(post => {
            console.log(post)
        })
    }, 1000)
}


async function init() {
    await createPost({
        title: 'post three',
        body: 'this is post three'
    })
    getPosts()
}

init()

// createPost({
//     "title" : "Post three",
//     "body"  : "this is post three"
// }).then(getPosts)
//     .catch(error => console.log(error))