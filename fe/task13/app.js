 //isiform class: Represent a is form
class isiform{
     constructor(name,price,store){
        this.name = name;
        this.price = price;
        this.store = store;
     }
}

 // UI class: Handle UI task
class UI 
    {
            static displayIsi()
        {
            const StoredIsi = 
            [
                {
                    name: 'lenovo',
                    price: 200000,
                    store: 'batam',
                },
                {
                    name: 'lenovo',
                    price: 400000,
                    store: 'bekasi',
                }
            ];

            const isi = StoredIsi;
            
            // isi.forEach(function(isi));
            isi.forEach((isi) => UI.addIsiToList(isi));
        }
        
        static addIsiToList(isi)
        {
            const list = document.querySelector('#form-list');

            const row = document.createElement('tr');

            row.innerHTML = `
                <td>${isi.name}</td>
                <td>${isi.price}</td>
                <td>${isi.store}</td>
                <td><a href='#' class="btn btn-danger btn-sm delete">X</a></td>
            `;
            list.appendChild(row);
        }

        static deleteisiform(el){
            if(el.classList.contains('delete')){
                el.parentElement.parentElement.remove();
            }
        }

        static showAlert(message, className){


        }


        static clearFields(){
            document.querySelector('#Name').value ='';
            document.querySelector('#Price').value ='';
            document.querySelector('#Store').value ='';
        };
    }
        

 //Store Class: Handle Storage






 
 //Event: Display Text Form
document.addEventListener(`DOMContentLoaded`,UI.displayIsi);

 //Event: Add text to Form
document.querySelector('#form-input').addEventListener('submit',(e) => 
    {
    
        //prevent actual submit
        e.preventDefault();
    
    
        //get form value
        const name =    document.querySelector('#Name').value;
        const price =   document.querySelector('#Price').value;
        const store =   document.querySelector('#Store').value;


        //validasi
        if(name === '' || price === ''|| store === ''){
            alert('silahkan isi di kotak yang tersedia.');
        } else{
            //instatiate isi
        const isi = new isiform(name, price, store);
    
        console.log(isi)

        //add isi to UI
        UI.addIsiToList(isi);

        //clear fields
        UI.clearFields();


        }
    
    });

 //Event: Remove text from Form
 document.querySelector('#form-list').addEventListener('click',(e) => {
    UI.deleteisiform (e.target)
 });