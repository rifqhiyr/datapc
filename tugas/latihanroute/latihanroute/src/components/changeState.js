import React, { Component } from 'react'

export default class changeState extends Component {
  state = {
    angka = 0
  };

  ubahAku =() => {
    this.setState({
      angka: this.state.angka + 1
    });
  };



  
  render() {
    return (
      <div>
        <h1>Latihan envent dan changeState</h1>
        <div>{this.state.angka}</div>
        <button onClick={this.ubahAku}>ubah aku</button>
      </div>
    );
  }
}
