import React, { Component } from "react";

class BookItem extends Component {
  render() {
    const books = this.props.books.map(books => {
      const { image, author, title, desc } = books;

      return (
        <div>
          <img src={image} alt="" />
          <h1>title:{title}</h1>
          <h1>desc:{desc}</h1>
          <h1>author:{author}</h1>
        </div>
      );
    });
    return <div> {books} </div>;
  }
}

export default BookItem;
