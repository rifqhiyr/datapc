import React, { Component } from "react";
import BookItem from "./BookItem";

class Booklist extends Component {
  render() {
    return (
      <div>
        <BookItem books={this.props.books} />
      </div>
    );
  }
}

export default Booklist;
