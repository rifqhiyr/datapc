import React, { Component } from "react";
// import Booklist from "../src/components/Booklist";
// import GambarPertama from "./images/bumimanusia_.jpg";
import { BrowserRouter as Router, Route } from "react-router-dom";
import HomePage from "./components/views/HomePage";
import AboutPage from "./components/views/AboutPage";

class App extends Component {
  state = {
    books: [
      {
        title: "Bumi Manusia",
        image: require("./images/bumimanusia_.jpg"),
        author: "Pramoedya Ananta Toer",
        desc:
          "Pram menggambarkan sebuah adegan antara Minke dengan ayahnya yang sangat sentimentil: Aku mengangkat sembah sebagaimana biasa aku lihat dilakukan punggawa terhadap kakekku dan nenekku dan orangtuaku, waktu lebaran. Dan yang sekarang tak juga kuturunkan sebelum Bupati itu duduk enak di tempatnya. Dalam mengangkat sembah serasa hilang seluruh ilmu dan pengetahuan yang kupelajari tahun demi tahun belakangan ini. Hilang indahnya dunia sebagaimana dijanjikan oleh kemajuan ilmu .... Sembah pengagungan pada leluhur dan pembesar melalui perendahan dan penghinaan diri! Sampai sedatar tanah kalau mungkin! Uh, anak-cucuku tak kurelakan menjalani kehinaan ini.",
        year: "January, 1980"
      },
      {
        title: "Jejak Langkah",
        image: require("./images/jejaklangkah.jpg"),
        author: "Pramoedya Ananta Toer",
        desc:
          "Minke memobilisasi segala daya untuk melawan bercokolnya kekuasaan Hindia yang sudah berabad-abad umurnya. Namun Minke tak pilih perlawanan bersenjata. Ia memilih jalan jurnalistik dengan membuat sebanyak-banyaknya bacaan Pribumi. Yang paling terkenal tentu saja Medan Prijaji.",
        year: "1985"
      },
      {
        title: "Gadis Pantai",
        image: require("./images/gadispantai.jpg"),
        author: "Pramoedya Ananta Toer",
        desc:
          "Roman ini menusuk feodalisme Jawa yang tak memiliki adab dan jiwa kemanusiaan tepat langsung di jantungnya yang paling dalam.",
        year: "1962"
      }
    ]
  };

  render() {
    return (
      <Router>
        <Route exact path="/" component={HomePage} />
        <Route path="/about" component={AboutPage} />

        {/* <Booklist books={this.state.books} /> */}
      </Router>
    );
  }
}

export default App;
