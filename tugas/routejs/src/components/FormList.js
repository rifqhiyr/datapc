import React, { Component } from "react";

export default class FormList extends Component {
  constructor() {
    super();
    this.state = { username: "", hasilubah: "" };
  }

  berubah = e => {
    this.setState({ hasilubah: e.target.value });
  };

  klik = () => {
    this.setState({ username: this.state.hasilubah });
  };

  render() {
    return (
      <div>
        <form>
          <label htmlFor="username">username</label>
          <input
            type="text"
            value={this.state.hasilubah}
            onChange={this.berubah}
          />
        </form>

        <button onClick={this.klik}>berubah</button>

        <h3>{this.state.username}</h3>
      </div>
    );
  }
}

// import React, { Component } from "react";

// export default class FormList extends Component {
//   constructor(props) {
//     super(props);
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }
//   handleSubmit(e) {
//     alert("the value is:" + this.input.value);
//     e.preventDefault();
//   }

//   render() {
//     return (
//       <form onSubmit={this.handleSubmit}>
//         <label>
//           Name:
//           <input type="text" ref={input => (this.input = input)} />
//         </label>
//         <input type="submit" value="Submit" />
//       </form>
//     );
//   }
// }
