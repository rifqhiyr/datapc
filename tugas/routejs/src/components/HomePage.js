import React, { Component } from "react";
import { Link } from "react-router-dom";

class HomePage extends Component {
  render() {
    return (
      <div>
        <h1>This is HomePage</h1>
        <Link to="/anime">
          <button>to Anime</button>
        </Link>
      </div>
    );
  }
}

export default HomePage;
