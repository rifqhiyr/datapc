import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class AnimeDetail extends Component {
  render() {
    const list = this.props.animes.map(anime => {
      return (
        <div key={anime.id}>
          <h1>
            {anime.title} : {anime.id}
          </h1>
          <Link to={`/${anime.id}`}>view Detail</Link>
        </div>
      );
    });
    return <div>{list}</div>;
  }
}
