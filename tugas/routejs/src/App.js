import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import AboutPage from "./components/AboutPage";
import AnimeDetail from "./components/AnimeDetail";
import DetailAnime from "./components/DetailAnime";
import FormList from "./components/FormList";

export default class App extends Component {
  state = {
    animelist: [
      {
        id: 1,
        title: "yu gi oh"
      },
      {
        id: 2,
        title: "dragon ball"
      }
    ]
  };
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" component component={FormList} />
          <Route path="/about" component={AboutPage} />
          <Route
            path="/anime"
            render={props => (
              <AnimeDetail {...props} animes={this.state.animelist} />
            )}
          />
          <Route path="/:id" component={DetailAnime} />
          {/* <Route path="/form" component={FormList} /> */}
        </Switch>
      </Router>
    );
  }
}
