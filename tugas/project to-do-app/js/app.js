

// cara membuat element Elements
const clear = document.querySelector(".clear");
const dateElement = document.getElementById("date");
const list = document.getElementById("list");
const input = document.getElementById("input");

// menamai setiap class
const CHECK = "fa-check-circle";
const UNCHECK = "fa-circle-thin";
const LINE_THROUGH = "lineThrough";

// Variable
let LIST, id;

// get item from localstorage
let data = localStorage.getItem("TODO");

// mengecek apakah data kosong atau tidak
if(data){
    LIST = JSON.parse(data);
    id = LIST.length; // set id ke daftar paling terakhir di list
    loadList(LIST); // memanggil list
}else{
    // yang dijalankan jika data kosong
    LIST = [];
    id = 0;
}

// Menambahkan parameter Item ke dalam ToDo list
function loadList(array){
    array.forEach(function(item){
        addToDo(item.name, item.id, item.done, item.trash);
    });
}

// Membuat method hapus semua
clear.addEventListener("click", function(){
    localStorage.clear();
    location.reload();
});

// Cara menampilkan Jam
const options = {weekday : "long", month:"short", day:"numeric"};
const today = new Date();

dateElement.innerHTML = today.toLocaleDateString("en-US", options);

// menambahkan fungsi ToDo

function addToDo(toDo, id, done, trash){
    
    if(trash){ return; }
    
    const DONE = done ? CHECK : UNCHECK;
    const LINE = done ? LINE_THROUGH : "";
    
    const item = `<li class="item">
                    <i class="fa ${DONE} co" job="complete" id="${id}"></i>
                    <p class="text ${LINE}">${toDo}</p>
                    <i class="fa fa-trash-o de" job="delete" id="${id}"></i>
                  </li>
                `;
    
    const position = "beforeend";
    
    list.insertAdjacentHTML(position, item);
}

// menambahkan item dengan cara memencet tombol enter
document.addEventListener("keyup",function(even){
    if(event.keyCode == 13){
        const toDo = input.value;
        
        // kalo input ada isinya
        if(toDo){
            addToDo(toDo, id, false, false);
            
            LIST.push({
                name : toDo,
                id : id,
                done : false,
                trash : false
            });
            
            // add item to localstorage ( this code must be added where the LIST array is updated)
            localStorage.setItem("TODO", JSON.stringify(LIST));
            
            id++;
        }
        input.value = "";
    }
});


// membuat fungsi complete to do
function completeToDo(element){
    element.classList.toggle(CHECK);
    element.classList.toggle(UNCHECK);
    element.parentNode.querySelector(".text").classList.toggle(LINE_THROUGH);
    
    LIST[element.id].done = LIST[element.id].done ? false : true;
}

// menghapus fungsi to do
function removeToDo(element){
    element.parentNode.parentNode.removeChild(element.parentNode);
    
    LIST[element.id].trash = true;
}

// membuat agar item dinamic

list.addEventListener("click", function(event){
    const element = event.target; //menampilkan fungsi clikc list
    const elementJob = element.attributes.job.value; // complete or delete
    
    if(elementJob == "complete"){
        completeToDo(element);
    }else if(elementJob == "delete"){
        removeToDo(element);
    }
    
    // menambahkan item ke localstorage ( kode ini mesti dithbahkan jika list array ditambah)
    localStorage.setItem("TODO", JSON.stringify(LIST));
});


















