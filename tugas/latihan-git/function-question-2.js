/*
Buatlah sebuah fungsi yang akan mencetak angka 1-30 ke console.Ketika angkanya habis di bagi 2 di depan angkanya akan di tambah string "Forza". Ketika angkanya habis di bagi 5, di depan angkanya di tambah string "Milan". Ketika angkanya habis di bagi 2 dan 5, di depan angkanya di tambah string "Forza Milan".
*/
function printNumber() {
  for (let i = 1; i <= 30; i++) {
    if (i % 2 === 0 && i % 5 === 0) {
      console.log(`Forza Milan ${i}`);
    } else if (i % 2 === 0) {
      console.log(`Forza ${i}`);
    } else if (i % 5 === 0) {
      console.log(`Milan ${i}`);
    } else {
      console.log(i);
    }
  }
}
printNumber();


console.log("--------------------------------------");
console.log("--------------------------------------");

/*
Kamu diminta untuk memprogram suatu game sederhana,GlintsWarrior namanya.
Untuk memulai game itu diperlukan 2 inputan yaitu nama dan peran.
Jika nama kosong beri pesan ke user untuk memasukkan nama, bilan peran kosong beri pesan ke user bahwa dia harus memilih peran
Terdapat 3 peran berbeda yaitu Ksatria, Tabib, dan Penyihir.
Jika nama dan peran nya ada maka beri pesan ke user sesuai dengan peran masing-masing
contoh: "slamat datang di glint warrior yudi, peran kamau adalah tabib"
*/
function glintsWarrior(name, character) {
  let a = name;
  let b = character;
  if (a === "") {
    console.log(`Input Your Name`);
  } else if (a === name && b === "") {
    console.log(`Choose Your Character? Knight, Healer, Mage?`);
  } else if (a === name && b === character) {
    console.log(`Welcome to Glints Warrior ${name}, your character is ${character}!`);
  }
}
glintsWarrior("Fresly", "Knight");

console.log("--------------------------------------");
console.log("--------------------------------------");

/*
Implementasikan function 'ubahString' untuk mengganti angka-angka yang ada di dalam 'str' menjadi sebuah huruf yang sesuai dengan aturan:
1 = i
4 = a
3 = e
7 = u
0 = o

contoh input-output:
y7d1 --> yudi
*/

function changeString(str) {
  let x = "";
  for (j = 0; j < str.length; j++) {
    if (str[j] === "1") {
      x += "i";
    } else if (str[j] === "4") {
      x += "a";
    } else if (str[j] === "3") {
      x += "e";
    } else if (str[j] === "7") {
      x += "u";
    } else if (str[j] === "0") {
      x += "o";
    } else {
      x += str[j];
    }
  }
  console.log(x);
}
changeString("y7d1");
