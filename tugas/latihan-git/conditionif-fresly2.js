/*
Karoke diskon membersip
1. Jika member tipenya bronze tampilkan "Biaya yang harus kamu bayar adalah 300rb"
2. Jika member tipenya silver tampilkan "Biaya yang harus kamu bayar adalah 400rb"
3. Jika member tipenya gold tampilkan "Biaya yang harus kamu bayar adalah 500rb"
4. Jika bukan member bernyanyi 1 jam tampilkan "Biaya yang harus kamu bayar adalah 50rb"
5. Jika bukan member bernyanyi 2 jam tampilkan "Biaya yang harus kamu bayar adalah 100rb"
6. Jika bukan member bernyanyi 3 jam tampilkan "Biaya yang harus kamu bayar adalah 150rb"
*/

let member = "notmember";
let rent = 3;

if (member === "bronze") {
  console.log("Biaya yang harus kamu bayar adalah 300rb");
} else if (member === "silver") {
  console.log("Biaya yang harus kamu bayar adalah 400rb");
} else if (member === "gold") {
  console.log("Biaya yang harus kamu bayar adalah 500rb");
} else {
  if (member === "notmember" && rent === 1) {
    console.log("Biaya yang harus kamu bayar adalah 50rb");
  } else if (member === "notmember" && rent === 2) {
    console.log("Biaya yang harus kamu bayar adalah 100rb");
  } else if (member === "notmember" && rent === 3) {
    console.log("Biaya yang harus kamu bayar adalah 150rb");
  } else {
    console.log("Tidak ada dalam daftar");
  }
}