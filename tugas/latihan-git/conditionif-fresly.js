/*
Di berikan sebuah angka !
1. Kalau angkanya ganjil tampilkan di layar "ganjil"
2. Kalau angkanya genap dan berada di antara 2 dan 5 tampilkan "OK"
3. Kalau angkanya genap dan berada di antara 6 dan 20 tampilkan "Tchuss"
4. Kalau angkanya genap dan berada lebih besar dari 20 tampilkan "Nihao"

contoh input dan output
3 --> ganjil
4 --> OK
17 --> ganjil
12 --> Tchuss
21 --> Nihao
*/

let a = 22;

if (a%2===1) {
  console.log("Ganjil");
} else {
  if (a>=2 && a<=5) {
    console.log("OK");
  } else if (a>=6 && a<=20) {
    console.log("Tchuss");
  } else {
    console.log("Nihao");
  }
}