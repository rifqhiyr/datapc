// ### No.4

// Diberikan sebuah array yang berisi kumpulan angka bilangan bulat positif.Buatlah sebuah fungsi yang menerima parameter array yang akan mengembalikan total penjumlahan dari setiap elemen - elemen array tersebut.

// ```bash
// var num = [4, 2, 8, 6]

// #output
// 20
// ```
console.log("-----------------------------------------");
console.log("----------------soal 4-------------------");

let number = [1, 2, 3, 4];
let total = 0;
function sumArray(a) {
  for (let i = 0; i < a.length; i++) {
    total += a[i];
  }
  console.log(total);
}
sumArray(number);

console.log("-----------------------------------------");
console.log("-----------------------------------------");

// ### No.5

// Diberikan 2 buah array yang berisi angka.Array tersebut memiliki panjang yang sama sama.Buatlah sebuah fungsi yang membandingkan setiap elemen dari ke dua array tersebut.
// Fungsi akan mengembalikan score hasil perbandingan ke dua array terbut.

// ```bash
// var numA = [2, 3, 5]
// var numB = [4, 1, 6]

// #output
// [1, 2]
// ```
console.log("-----------------------------------------");
console.log("----------------soal 5-------------------");

let numberA = [6, 3, 2];
let numberB = [4, 5, 1];

function compareNumber(v, w) {
  let numbA = 0;
  let numbB = 0;
  let compare = [];

  for (let j=0; j<v.length; j++) {
    // console.log(`${v[j]} ${w[j]}`);
    if (v[j] > w[j]) {
      numbA += 1;
    } else if (v[j] < w[j]) {
      numbB += 1;
    }
  }
  // console.log(`${numbA} ${numbB}`);
  compare.push(numbA, numbB);
  console.log(compare);
}
compareNumber(numberA, numberB)

console.log("-----------------------------------------");
console.log("-----------------------------------------");

// ### No.6

// Diberikan sebuah array yang berisi 5 buah bilangan bulat positif.Cari nilai minimum dan maksimum yang dapat dihitung dengan menjumlahkan tepat empat elemen dari lima elemen array tersebut.

// ```bash
// var num = [2, 3, 5, 1, 4]

// #output
// [10,14]
// ```
console.log("-----------------------------------------");
console.log("----------------soal 6-------------------");

let numberMinMax = [2, 3, 5, 1, 4];
function minmaxSum(xoxo) {
  let totalNumber = 0;
  for (let i=0; i<xoxo.length; i++) {
    totalNumber += xoxo[i];
  }
  let minNumber = totalNumber;
  let maxNumber = totalNumber;
  function minNumb(xoxoMin) {
    minNumber -= Math.max.apply(null, xoxoMin);
  }
  (minNumb(numberMinMax));
  function maxNumb(xoxoMax) {
    maxNumber -= Math.min.apply(null, xoxoMax);
  }
  maxNumb(numberMinMax);
  console.log(`Minimum Points is ${minNumber}`);
  console.log(`Maximum Points is ${maxNumber}`);
}
minmaxSum(numberMinMax)


console.log("-----------------------------------------");
console.log("-----------------------------------------");

// ### No.7

// Anda diberi daftar bilangan bulat n - 1 dan bilangan bulat ini berada dalam kisaran 1 hingga n.Tidak ada duplikat dalam daftar hanya saja angkanya akan di acak.Salah satu bilangan bulat tidak ada dalam daftar.Buatlah sebuah fungsi yang akan mengembalikan angka yang hilang tersebut.

// ```bash
// var num = [1, 3, 4, 2, 7, 6]

// #output
// 5
// ```

console.log("-----------------------------------------");
console.log("----------------soal 7-------------------");

let numberC = [8, 3, 4, 7, 6, 1];

function backNumber(abc) {
  let numberNum = [];
  let numberNumBack = [];
  numberNum = abc.sort(function(a, b){return a-b});
  for (let l = Math.min.apply(null, numberNum); l <= Math.max.apply(null, numberNum); l++) {
    // console.log(`array l --> ${l}`);

    for (let m=0; m<=numberNum.length; m++) {
      // console.log(`array m --> ${numberNum[m]}`);

      if (l === numberNum[m]) {
        // console.log(`${l} = ${numberNum[m]}`);
        // console.log(`Berhasil`);
        // console.log(`----`);
        break;
      } else {
        if (numberNum[m] === undefined) {
          numberNumBack.push(l);
          // console.log(`Berhasil di push ke array number yang tidak ada`);
        } else {
          // console.log(`Salah`);
        }
      }
    }
  }
  console.log(numberNumBack);
}
backNumber(numberC);

console.log("-----------------------------------------");
console.log("-----------------------------------------");
