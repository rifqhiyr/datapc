// ### No.1

// Buatlah sebuah fungsi yang akan mencetak angka 1 - 30 ke console.Ketika angkanya habis di bagi 2 di depan angkanya akan di tambah string "Forza".Ketika angkanya habis di bagi 5, di depan angkanya di tambah string "Milan".Ketika angkanya habis di bagi 2 dan 5, di depan angkanya di tambah string "Forza Milan".

// ```bash
// #output
// 1
// Forza 2
// ...
// Milan 5
// ...
// Forza Milan 10
// ```

function forzaMilan(number) {
  for (let i = 1; i <= number; i++) {
    // condition
    if (i % 2 === 0 && i % 5 === 0) { console.log(`Forza Milan ${i}`) }
    if (i % 2 === 0) { console.log(`Forza ${i}`) }
    if (i % 5 === 0) { console.log(`Milan ${i}`) }
  }
}
forzaMilan(30);



// ### No.2

// #### Kamu diminta untuk memprogram suatu game sederhana, GlintsWarrior namanya.

// #### Untuk memulai game itu diperlukan 2 variabel(untuk sekarang), yaitu nama dan peran.

// #### Variabel peran harus memiliki isi data, bila kosong pemain akan diberikan peringatan berupa "Pilih Peranmu untuk memulai game".

// #### Terdapat 3 peran berbeda yaitu Ksatria, Tabib, dan Penyihir.

// #### Tugas kamu adalah untuk membuat program yang mengecek isi variabel peran serta mengeluarkan respon sesuai isi variabel tersebut.

// #### Petunjuk soal:

// - Variabel tetap di - input secara manual.
// - Variabel nama dan peran dapat diisi apa saja.
// - Nama tidak perlu dicek sama persis seperti contoh input / output
//   - Lakukan pengecekan setiap peran menggunkan guard clause

//     ```javascript
// //contoh input
// var nama = "yudi";
// var peran = "";

// //Silakan ganti nilai nama dan peran untuk mengetes kondisi!

// // Output untuk Input nama = '' dan peran = ''
// ("Nama harus diisi!");

// //Output untuk Input nama = 'Danu' dan peran = ''
// ("Halo Danu, Pilih peranmu untuk memulai game!");

// //Output untuk Input nama = Maria  dan peran 'Ksatria'
// ("Selamat datang di Dunia GlintsWarrior, Maria");
// ("Halo Ksatria Maria, Tugas kamu adalah menyerang musuh!");

// //Output untuk Input nama = Badai  dan peran 'Tabib'
// ("Selamat datang di Dunia GlintsWarrior , Badai ");
// ("Halo Tabib Badai, kamu akan membantu temanmu yang terluka.");

// //Output untuk Input nama = ‘Hendri  dan peran 'Penyihir'
// ("Selamat datang di Dunia GlintsWarrior, Hendri ");
// ("Halo Penyihir Hendri, ciptakan keajaiban yang membantu kemenanganmu!");
// ```

const glintsWarrior = function (name = "", peran = "") {
  if (!name) {
    console.log(`Kamu harus is nama dulu ya!`);
  } else {
    if (!peran) {
      console.log(`Hay ${name} kamu harus memilih peran!`);
    } else {
      switch (peran) {
        case "Ksatria":
          console.log(`Hay ${name}, peran kamu adalah ${peran}!`);
          break;
        case "Tabib":
          console.log(`Hay ${name}, peran kamu adalah ${peran}!`);
          break;
        case "Penyihir":
          console.log(`Hay ${name}, peran kamu adalah ${peran}!`);
          break;
        default:
          console.log(`Hay ${name}, peran ini tidak tersedia!`)
          break;
      }
    }
  }
}
glintsWarrior("Fresly", "Ksatria")




// ### No.3

// #### Implementasikan function 'ubahString' untuk mengganti angka - angka yang ada di dalam 'str' menjadi sebuah huruf yang sesuai dengan aturan:

// - 1 = i
//   - 4 = a
//     - 3 = e
//       - 7 = u
//         - 0 = o

//           ```javascript
// // Test cases
// // gunakan arrow function dan atau guard clause untuk latihan ini !
// console.log(numberLetters("prat1w1n7r4m1n1")); // pratiwinuramini
// console.log(numberLetters("y7d1kr1sn4nd1")); // mas yudi
// console.log(numberLetters("b4d41")); // badai
// ```

const ubahString = str => {
  let hasil = "";
  for (let i = 0; i < str.length; i++) {
    switch (str[i]) {
      case "1":
        hasil += "i";
        break;
      case "4":
        hasil += "a";
        break;
      case "3":
        hasil += "e";
        break;
      case "7":
        hasil += "u";
        break;
      case "0":
        hasil += "o";
        break;
      default:
        hasil += str[i];
        break;
    }
  }
  console.log(`Dari ${str} --> ${hasil}`);
}
ubahString("fr3sly l7mb4n");



// ### No.4

// Diberikan sebuah array yang berisi kumpulan angka bilangan bulat positif.Buatlah sebuah fungsi yang menerima parameter array yang akan mengembalikan total penjumlahan dari setiap elemen - elemen array tersebut.

// ```bash
// var num = [4, 2, 8, 6]

// #output
// 20
// ```

const findTotal = arr => {
  let total = 0;
  for (let i = 0; i < arr.length; i++) {
    total += arr[i];
  }
  return total;
}
console.log(findTotal([4, 2, 8, 6]));




// ### No.5

// Diberikan 2 buah array yang berisi angka.Array tersebut memiliki panjang yang sama sama.Buatlah sebuah fungsi yang membandingkan setiap elemen dari ke dua array tersebut.
// Fungsi akan mengembalikan score hasil perbandingan ke dua array terbut.

// ```bash
// var numA = [2, 3, 5]
// var numB = [4, 1, 6]

// #output
// [1, 2]
// ```

const compareArray = (arr1, arr2) => {
  let score = [0, 0];
  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] > arr2[i]) {
      score[0]++;
    } else {
      score[1]++;
    }
  }
  return score;
}
console.log(compareArray([2, 3, 5], [4, 1, 6]));




// ### No.6

// Diberikan sebuah array yang berisi 5 buah bilangan bulat positif.Cari nilai minimum dan maksimum yang dapat dihitung dengan menjumlahkan tepat empat elemen dari lima elemen array tersebut.

// ```bash
// var num = [2, 3, 5, 1, 4]

// #output
// [10,14]
// ```
// TODO: Not Testing!

const minMax = (arr) => {
  let sorted = arr.sort(function (a, b) { return a - b });
  let total = findTotal(arr);
  let len = arr.length;
  let aB = [0, 0];
  aB[0] = total - sorted[len - 1];
  aB[1] = total - sorted[0];
  return aB;
}
console.log(minMax([2, 1, 3, 5, 4]));


// ### No.7

// Anda diberi daftar bilangan bulat n - 1 dan bilangan bulat ini berada dalam kisaran 1 hingga n.Tidak ada duplikat dalam daftar hanya saja angkanya akan di acak.Salah satu bilangan bulat tidak ada dalam daftar.Buatlah sebuah fungsi yang akan mengembalikan angka yang hilang tersebut.

// ```bash
// var num = [1, 3, 4, 2, 7, 6]

// #output
// 5
// ```

const findMissNumber = arr => {
  let n = arr.length;
  let totalWithoutMissNumber = findTotal(arr);
  let total = (n + 1) * (n + 2) / 2;
  return total - totalWithoutMissNumber;
}
console.log(findMissNumber([1, 3, 4, 2, 7, 6]));
