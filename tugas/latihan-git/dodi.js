//If Else
//============Soal 1========================

function cekAngka(angka){

  if (angka % 2 !== 0){
    console.log("ganjil")
  }
    else{
      if(angka >= 2 && angka <= 5 ){
        console.log("ok")
      }
      else if (angka >= 6 && angka <= 20 ){
        console.log("Tchuss")
      }
      else if (angka > 20 ){
        console.log("Nihao")
      }
    }
}

console.log(cekAngka(20))


// //===========Soal 2===========================

function karaoke(member, hour){

  if (member !== ""){
    if (member === "bronze"){
      console.log("Biaya yang harus kamu bayar adalah 300rb")
    }
    else if(member === "silver"){
      console.log("Biaya yang harus kamu bayar adalah 400rb")
    }
    else if(member === "gold"){
      console.log("Biaya yang harus kamu bayar adalah 500rb")
    }
  }
  else {
    if(hour === 1){
      console.log("Biaya yang harus kamu bayar adalah 50rb")
    }
    else if(hour === 2){
      console.log("Biaya yang harus kamu bayar adalah 100rb")
    }
    else if(hour === 3){
      console.log("Biaya yang harus kamu bayar adalah 150rb")
    }
  }
}

console.log(karaoke("", 2))


// //==============Soal 3============================
  function cetakAngka(angka){
    if (angka >=1 && angka <=30){
      for (let i = 1; i <= angka; i++){
        if (i % 2 === 0 && i % 5 === 0){
          console.log("Forza Milan " + i)
        }
        else if (i % 2 === 0){
          console.log("Forza " + i)
        }
        else if (i % 5 === 0){
          console.log("Milan " + i)
        }
      }
    }
    else{
      console.log("angka diluar range")
    }
  }

  cetakAngka(10)

//==============Soal 4============================
function glintsWarrior(nama, peran){
  if(nama === "" && peran === ""){
    console.log("masukkan nama & peran")
  }
  else if (nama !== "" && peran === ""){
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
  }
  else if(peran === "Ksatria"){
    console.log("Selamat datang di Dunia GlintsWarrior, " + nama)
    console.log("Halo Ksatria " + nama + ", Tugas kamu adalah menyerang musuh!")
  }
  else if(peran === "Tabib"){
    console.log("Selamat datang di Dunia GlintsWarrior, " + nama)
    console.log("Halo Tabib " + nama + ", kamu akan membantu temanmu yang terluka.")
  }
  else if(peran === "Penyihir"){
    console.log("Selamat datang di Dunia GlintsWarrior, " + nama)
    console.log("Halo Penyihir " + nama + ", cepatlah bertobat")
  }
}

glintsWarrior("Dodi","Ksatria")


// //==============Soal 5===========================

function ubahString(str){

  var tampungStr = ""

  for(let i = 0; i < str.length; i++){
    if (str[i] === "a"){
      tampungStr += 4
    }
    else if (str[i] === "i"){
      tampungStr += 1
    }
    else if (str[i] === "u"){
      tampungStr += 7
    }
    else if (str[i] === "e"){
      tampungStr += 3
    }
    else if (str[i] === "o"){
      tampungStr += 0
    }
    else{
      tampungStr += str[i]
    }
  }

  console.log(tampungStr)

}

ubahString("dodi wahyudi")

//switch version

function ubahString(str){

  for (let i = 0; i < str.length; i++){

    var tampung = ""

    switch(str[i]){

      case "1": tampung += "i" 
      break;
      case "4": tampung += "a"
      break;
      case "3": tampung += "e"
      break;
      case "7": tampung += "u"
      break;
      case "0": tampung += "o"
      break;
      default: tampung += str[i];

    }
    
  }

  console.log(tampung)
}

ubahString("d0d1 wahyudi")


//==============Soal 6===========================

let num = [4, 2, 8, 6]

function jumlahArr(arr){

  let hasil = 0

  for (let i = 0; i < arr.length; i++){
    hasil += arr[i]
  }

  console.log (hasil)

}

jumlahArr(num)


//==============Soal 7===========================

let numA = [2, 3, 5]
let numB = [4, 1, 6]

function bandingAngka(arr1, arr2){

  let countA = 0
  let countB = 0
  let output = []

  for (let i = 0; i < numA.length; i++){

    if (arr1[i] > arr2[i]){
      countA += 1
    }
    else{
      countB += 1
    }

  }

  output.push(countA, countB)

  console.log(output)

}

bandingAngka(numA, numB)

// ==============Soal 8===========================

let num = [2, 3, 5, 1, 4]

function minMax(arr){

  let tampung = []
  let hasilJumlah = 0
  output = 0 

  for(let i = 0; i < arr.length; i++){

      hasilJumlah += arr[i]

  }

  for(let j = 1; j <= arr.length; j++){

    output = hasilJumlah - j

    tampung.push(output)
    
  }

  console.log(tampung[4], tampung[0])

}

minMax(num)

//==============Soal 9===========================

let num = [1, 3, 4, 2, 7, 6]

function angkaHilang(arr){

  arr.sort()

  let pengurang = 0
  let tampungPengurang = []

  for (let i = arr.length -1; i > 0; i--){

    pengurang = arr[i] - arr[i - 1]
    tampungPengurang.push(pengurang) 
   
  }

  tampungPengurang.reverse()
 
  let tampil = tampungPengurang.indexOf(2) + 2
  
  console.log(tampil)

}

angkaHilang(num)

