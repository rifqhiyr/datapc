/*
Di berikan sebuah angka !
1. Kalau angkanya ganjil tampilkan di layar "ganjil"
2. Kalau angkanya genap dan berada di antara 2 dan 5 tampilkan "OK"
3. Kalau angkanya genap dan berada di antara 6 dan 20 tampilkan "Tchuss"
4. Kalau angkanya genap dan berada lebih besar dari 20 tampilkan "Nihao"

contoh input dan output
3 --> ganjil
4 --> OK
17 --> ganjil
12 --> Tchuss
21 --> Nihao
*/
// --------------------JAWAB---------------------------
// let angka = 3;

// if (angka%2 !== 0) {
//     console.log ("ganjil");
// } else if (angka%2 === 0 && angka>=2 && angka<=5) {
//     console.log ("OK");
// } else if (angka%2 === 0 && angka>=6 && angka<=20) {
//     console.log ("Tchuss");
// } else if (angka%2 === 0 && angka>=20) {
//     console.log ("nihao");
// }

// ===========================================================================================
/*
Karoke diskon membersip
1. Jika member tipenya bronze tampilkan "Biaya yang harus kamu bayar adalah 300rb"
2. Jika member tipenya silver tampilkan "Biaya yang harus kamu bayar adalah 400rb"
3. Jika member tipenya gold tampilkan "Biaya yang harus kamu bayar adalah 500rb"
4. Jika bukan member bernyanyi 1 jam tampilkan "Biaya yang harus kamu bayar adalah 50rb"
5. Jika bukan member bernyanyi 2 jam tampilkan "Biaya yang harus kamu bayar adalah 100rb"
6. Jika bukan member bernyanyi 3 jam tampilkan "Biaya yang harus kamu bayar adalah 150rb"
*/

// ------------------------JAWAB--------------------------

// function diskon (x,y) {
//     if (x === "bronze") {
//         console.log("Biaya yang harus kamu bayar adalah 300rb")
//     } else if (x === "silver") {
//         console.log("Biaya yang harus kamu bayar adalah 400rb")
//     } else if (x === "gold") {
//         console.log ("Biaya yang harus kamu bayar adalah 500rb")
//     } else if (x !== "bronze" && x !== "silver" && x !== "gold" && y === 1 ) {
//         console.log ("Biaya yang harus kamu bayar adalah 50rb")
//     } else if (x !== "bronze" && x !== "silver" && x !== "gold" && y === 2 ) {
//         console.log ("Biaya yang harus kamu bayar adalah 100rb")
//     } else if (x !== "bronze" && x !== "silver" && x !== "gold" && y === 3 ) {
//         console.log ("Biaya yang harus kamu bayar adalah 150rb")
//     } 
// }

// diskon("bukanMember", 2)

// =====================================================================================
/*
Buatlah sebuah fungsi yang akan mencetak angka 1-30 ke console.Ketika angkanya habis di bagi 2 
di depan angkanya akan di tambah string "Forza". Ketika angkanya habis di bagi 5, 
di depan angkanya di tambah string "Milan". Ketika angkanya habis di bagi 2 dan 5, 
di depan angkanya di tambah string "Forza Milan".
*/

// ------------------------------JAWAB----------------------------------------
//  function angka (z) {
//      if (z>=1 && z<=30 && z%2 === 0) {
//         console.log(z +" "+"Forza")
//      } else if (z>=1 && z<=30 && z%5 === 0) {
//          console.log(z +" "+"Milan")
//      } else if (z>=1 && z<=30 && z%2 === 0 && z%5 === 0) {
//          console.log(z+" "+"Forza Milan")
//      } else {console.log("Bukan siapa siapa")}
//  }

//  angka (1)
//  ==============================================================================================
/*
Kamu diminta untuk memprogram suatu game sederhana,GlintsWarrior namanya.
Untuk memulai game itu diperlukan 2 inputan yaitu nama dan peran.
Jika nama kosong beri pesan ke user untuk memasukkan nama, bilan peran kosong 
beri pesan ke user bahwa dia harus memilih peran
Terdapat 3 peran berbeda yaitu Ksatria, Tabib, dan Penyihir.
Jika nama dan peran nya ada maka beri pesan ke user sesuai dengan peran masing-masing
contoh: "slamat datang di glint warrior yudi, peran kamau adalah tabib"
*/

// -----------------------------------JAWAB------------------------------------

// function game (nama,peran) {
//     if (nama === null && (peran === "Ksatria" ||peran === "Tabib" || peran === "Penyihir" )) {
//         console.log("Silahkan masukkan nama anda.")
//     } else if (nama !== null && peran === null ) {
//         console.log("Silahkan pilih peran.")
//     } else if (nama !== null && peran === "Penyihir") {
//         console.log("slamat datang di glint warrior " + nama +", peran kamau adalah Penyihir")
//     } else if (nama !== null && peran === "Tabib") {
//         console.log("slamat datang di glint warrior " + nama +", peran kamau adalah Tabib")
//     } else if (nama !== null && peran === "Ksatria") {
//         console.log("slamat datang di glint warrior " + nama +", peran kamau adalah Ksatria")
//     }
// }

// game(null,"Tabib")
// game("fian", null)

// ==============================================================================================
/*
Implementasikan function 'ubahString' untuk mengganti angka-angka yang ada 
di dalam 'str' menjadi sebuah huruf yang sesuai dengan aturan:
1 = i
4 = a
3 = e
7 = u
0 = o

contoh input-output:
y7d1 --> yudi
*/
// -------------------------------JAWAB---------------------------------------
// function ubahString (x) {
//         if (x) {
//             console.log(x.replace(1,"i"))
//         }
//     }
// ubahString ("f1an")
// -----------------------------------------
// String.prototype.allReplace = function(obj) {
//     var retStr = this;
//     for (var x in obj) {
//         retStr = retStr.replace(new RegExp(x, 'g'), obj[x]);
//     }
//     return retStr;
// };


// var z = "f14n"
// console.log(z.allReplace({1: 'i', 4: 'a', 3: 'e', 7: 'u', 0: 'o'}));
// ---------------------------------------------------

// function ubahString (x) {
//     if (x) {
//         console.log (x.replace(/1/g,'i').replace(/4/g,'a').
//         replace(/3/g,'e').replace(/7/g,'u').replace(/0/g,'o'))
//     }
// }
// ubahString ("f14ny7")


// ===============================================================================
//TUGAS MEMBUAT STRUKTUR DATA

// let binarClass = [
//     {
//         picture: "/images/icon1.png",
//         className: "iOS Engineer",
//         classDesc: "You will learn to design your project architecture on this platform. Furthermore, you will also be challenged to deploy your project app on Appstore."
//     },
//     {
//         picture: "/images/icon2.png",
//         className: "Backend Engineer",
//         classDesc: "Tons of database creation languages will be your breakfast before you creating its ramork architecture. You will get used to write API."
//     },
//     {
//         picture: "images/icon3.png",
//         className: "Frontend Web",
//         classDesc: "After taking this class, HTML and CSS will be your best friends. Consequently, you will also learn how to operate Javascript and design its framework as well"
//     },
//     {
//         picture: "images/icon4.png",
//         className: "UI/UX Designer",
//         classDesc: "You will not only learn to master design technique, but also to establish user-oriented mindset that makes you sensitive to user needs."
//     },
//     {
//         picture: "images/icon5.png",
//         className: "Android Engineer",
//         classDesc: "You will learn from its basic structures, architecture construction, to the release of your project APK on Playstore. Interesting, isn't it?"
//     },
//     {
//         picture: "images/icon6.png",
//         className: "Quality Assurance",
//         classDesc: "You will learn to find bugs and do multiple test to minimize errors on your project app. If you are good at finding flaws and mistakes, this class for you!"
//     }
// ]

//         for (let i=0; i<binarClass.length; i++ ) {
//             console.log (binarClass[i].classDesc)
//         }

// ========================================SOAL NOMER 4=======================================================
// Diberikan sebuah array yang berisi kumpulan angka bilangan bulat positif. Buatlah sebuah fungsi 
// yang menerima parameter array yang akan mengembalikan total penjumlahan dari setiap elemen-elemen 
// array tersebut.

var num = [4, 2, 8, 6];
var total = 0;

function hitung (){
    for (i=0; i<num.length; i++){
        total += num[i];
    }
    console.log (total)
}

hitung ()

// =========================================SOAL NOMER 5===================================================
// Diberikan 2 buah array yang berisi angka.Array tersebut memiliki panjang yang sama sama. 
// Buatlah sebuah fungsi yang membandingkan setiap elemen dari ke dua array tersebut.
// Fungsi akan mengembalikan score hasil perbandingan ke dua array terbut.

var numA = [2, 3, 5]
var numB = [4, 1, 6]

function compare () {
    var besarA = 0;
    var besarB = 0;
    var arrayBanding =[];

    for (i=0; i<numA.length; i++) {

            if (numA[i] > numB[i]) {
              besarA += 1;
            } else { 
                besarB += 1;
            }
    }

    arrayBanding.push(besarA, besarB)
    console.log(arrayBanding)
}

compare ()

// ===========================SOAL NOMER 6==========================================
// Diberikan sebuah array yang berisi 5 buah bilangan bulat positif. 
// Cari nilai minimum dan maksimum yang dapat dihitung dengan menjumlahkan tepat empat elemen dari lima elemen array tersebut.

var num = [2, 3, 5, 1, 4]


function kecilBesar () {
if (num.sort(function(a,b){
    return a-b
})) {  
    var jumlahTerkecil = 0;
        for (i=0; i< (num.length-1); i++) {
            jumlahTerkecil += num[i]
            // console.log(jumlahTerkecil);

        }   
    } 
    
if (num.sort(function(a,b){
        return b-a
    })) {
        var jumlahTerbesar = 0;
        for (k=0; k< (num.length-1); k++) {
            jumlahTerbesar += num[k]
            // console.log (jumlahTerbesar);
        } 
    }

   console.log([jumlahTerkecil,jumlahTerbesar]);
}

kecilBesar()

// ===========================SOAL NOMER 7==========================================
// Anda diberi daftar bilangan bulat n-1 dan bilangan bulat ini berada dalam kisaran 1 hingga n. 
// Tidak ada duplikat dalam daftar hanya saja angkanya akan di acak. Salah satu bilangan bulat tidak ada dalam daftar. 
// Buatlah sebuah fungsi yang akan mengembalikan angka yang hilang tersebut.

var num = [0, 1, 3, 4, 2, 7, 6]

function angkaHilang (x) {
    var urutArray = x.sort((num1, num2) => {
        return num1 - num2;
    });

    for (i=0; i<urutArray.length; i++) {
        if (i !== urutArray[i]) {
            return i;
        }
    }
}

console.log(angkaHilang (num))
// ==================================================================

