/*
Di berikan sebuah angka !
1. Kalau angkanya ganjil tampilkan di layar "ganjil"
2. Kalau angkanya genap dan berada di antara 2 dan 5 tampilkan "OK"
3. Kalau angkanya genap dan berada di antara 6 dan 20 tampilkan "Tchuss"
4. Kalau angkanya genap dan berada lebih besar dari 20 tampilkan "Nihao"

contoh input dan output
3 --> ganjil
4 --> OK
17 --> ganjil
12 --> Tchuss
21 --> ganjil
22 --> nihao
*/

let a = 6;
let angka = function (a) {
  if (a % 2 === 1) {
    console.log("Ganjil");
  } else {
    if (a >= 2 && a <= 5) {
      console.log("OK");
    } else if (a >= 6 && a <= 20) {
      console.log("Tchuss");
    } else {
      console.log("Nihao");
    }
  }
}
angka();



/*
Karoke diskon membersip
1. Jika member tipenya bronze tampilkan "Biaya yang harus kamu bayar adalah 300rb"
2. Jika member tipenya silver tampilkan "Biaya yang harus kamu bayar adalah 400rb"
3. Jika member tipenya gold tampilkan "Biaya yang harus kamu bayar adalah 500rb"
4. Jika bukan member bernyanyi 1 jam tampilkan "Biaya yang harus kamu bayar adalah 50rb"
5. Jika bukan member bernyanyi 2 jam tampilkan "Biaya yang harus kamu bayar adalah 100rb"
6. Jika bukan member bernyanyi 3 jam tampilkan "Biaya yang harus kamu bayar adalah 150rb"
*/

let member = "notmember";
let rent = 1;
let karaoke = function (member, rent) {
  if (member === "bronze") {
    console.log("Biaya yang harus kamu bayar adalah 300rb");
  } else if (member === "silver") {
    console.log("Biaya yang harus kamu bayar adalah 400rb");
  } else if (member === "gold") {
    console.log("Biaya yang harus kamu bayar adalah 500rb");
  } else {
    if (member === "notmember" && rent === 1) {
      console.log("Biaya yang harus kamu bayar adalah 50rb");
    } else if (member === "notmember" && rent === 2) {
      console.log("Biaya yang harus kamu bayar adalah 100rb");
    } else if (member === "notmember" && rent === 3) {
      console.log("Biaya yang harus kamu bayar adalah 150rb");
    } else {
      console.log("Tidak ada dalam daftar");
    }
  }
}
karaoke();
