//============ Soal 1 ==================//
/*
Buatlah sebuah fungsi yang akan mencetak angka 1-30 ke console.Ketika angkanya habis di bagi 2 di depan angkanya akan di tambah string "Forza". Ketika angkanya habis di bagi 5, di depan angkanya di tambah string "Milan". Ketika angkanya habis di bagi 2 dan 5, di depan angkanya di tambah string "Forza Milan".

```bash
#output
1
Forza 2
...
Milan 5
...
Forza Milan 10
```
*/


// const number = () => {
//   for (let i = 1; i <= 30; i++) {
//     // console.log (i);
//     if (i % 2 === 0) {
//       console.log(`Forza --- ${i}`);
//     } else if (i % 5 === 0) {
//       console.log(`Milan --- ${i}`);
//     } else {
//       console.log(`Forza Milan --- ${i}`);
//     }
//   }
// }

// number();


const pp = () => {
  for (let i = 0; i <= 30 ;i++) {
    if (i % 2 === 0 &&  i % 5 === 0) {
      console.log(`Forza Milan --- ${i}`);
    } else if (i % 2 === 0) {
      console.log(`forza --- ${i}`);
    } else if (i % 5 === 0) {
      console.log(`milan --- ${i}`);
  }
}
}
pp();


//============ Soal 2 ==================//
/*
#### Kamu diminta untuk memprogram suatu game sederhana,GlintsWarrior namanya.

#### Untuk memulai game itu diperlukan 2 variabel (untuk sekarang), yaitu nama dan peran.

#### Variabel peran harus memiliki isi data, bila kosong pemain akan diberikan peringatan berupa "Pilih Peranmu untuk memulai game".

#### Terdapat 3 peran berbeda yaitu Ksatria, Tabib, dan Penyihir.

#### Tugas kamu adalah untuk membuat program yang mengecek isi variabel peran serta mengeluarkan respon sesuai isi variabel tersebut.

#### Petunjuk soal :

- Variabel tetap di-input secara manual.
- Variabel nama dan peran dapat diisi apa saja.
- Nama tidak perlu dicek sama persis seperti contoh input/output
- Lakukan pengecekan setiap peran menggunkan guard clause

```javascript
//contoh input
var nama = "yudi";
var peran = "";

//Silakan ganti nilai nama dan peran untuk mengetes kondisi!

// Output untuk Input nama = '' dan peran = ''
("Nama harus diisi!");

//Output untuk Input nama = 'Danu' dan peran = ''
("Halo Danu, Pilih peranmu untuk memulai game!");

//Output untuk Input nama = Maria  dan peran 'Ksatria'
("Selamat datang di Dunia GlintsWarrior, Maria");
("Halo Ksatria Maria, Tugas kamu adalah menyerang musuh!");

//Output untuk Input nama = Badai  dan peran 'Tabib'
("Selamat datang di Dunia GlintsWarrior , Badai ");
("Halo Tabib Badai, kamu akan membantu temanmu yang terluka.");

//Output untuk Input nama = ‘Hendri  dan peran 'Penyihir'
("Selamat datang di Dunia GlintsWarrior, Hendri ");
("Halo Penyihir Hendri, ciptakan keajaiban yang membantu kemenanganmu!");
```
*/

var name = "Danu";
var peran = "Tabib";

if (name === "") {
  console.log("Nama harus diisi!");
} else if (name === "Danu" && peran === "") {
  console.log("Halo Danu, Pilih peranmu untuk memulai game!");
} else if (name === "Danu" && peran === "Ksatria") {
  console.log("Selamat datang di Dunia GlintsWarrior, Danu");
  console.log("Halo Ksatria Danu, Tugas kamu adalah menyerang musuh!");
} else if (name === "Danu" && peran === "Tabib") {
  console.log("Selamat datang di Dunia GlintsWarrior , Danu");
  console.log("Halo Tabib Danu, kamu akan membantu temanmu yang terluka.");
} else if (name === "Danu" && peran === "Penyihir") {
  console.log("Selamat datang di Dunia GlintsWarrior, Hendri");
  console.log("Halo Penyihir Hendri, ciptakan keajaiban yang membantu kemenanganmu!");
} else {
  console.log("Please choose your character!");
}



//============ Soal 3 ==================//
/*
#### Implementasikan function 'ubahString' untuk mengganti angka-angka yang ada di dalam 'str' menjadi sebuah huruf yang sesuai dengan aturan:

- 1 = i
- 4 = a
- 3 = e
- 7 = u
- 0 = o

```javascript
*/


// const ubahString = (str) => {
//   let tampung = "";
//   for (let i = 0; i < str.length; i++) {
//     // console.log (str[i]);
//     if (str[i] === "1") {
//       tampung += "i";
//     } else if (str[i] === "4") {
//       tampung += "a";
//     } else if (str[i] === "3") {
//       tampung += "e";
//     } else if (str[i] === "7") {
//       tampung += "u";
//     } else if (str[i] === "0") {
//       tampung += "0";
//     } else {
//       tampung += str[i];
//     }
//   }
//   return tampung;
// }

// //Test Case
// console.log(ubahString("prat1w1n7r4m1n1")); // pratiwinuramini
// console.log(ubahString("y7d1kr1sn4nd1")); // mas yudi
// console.log(ubahString("b4d41")); // badai



const nameChange = (str) => {
  let tampung = "";
  for (let i = 0; i < str.length; i++) {
    // console.log (str[i]);
    switch (str[i]) {
      case "1" :
      tampung += "i"; break;
      case "4" :
      tampung += "a"; break;
      case "3" :
      tampung += "e"; break;
      case "7" :
      tampung += "u"; break;
      case "0" :
      tampung += "o"; break;
      default :
      tampung += str[i]; break;
    }
  }
  return tampung;
}


//Test Case
console.log(nameChange("prat1w1n7r4m1n1")); // pratiwinuramini
console.log(nameChange("y7d1kr1sn4nd1")); // mas yudi
console.log(nameChange("b4d41")); // badai




//============ Soal 4 ==================//
/*
Diberikan sebuah array yang berisi kumpulan angka bilangan bulat positif. Buatlah sebuah fungsi yang menerima parameter array yang akan mengembalikan total penjumlahan dari setiap elemen-elemen array tersebut.

```bash
var num = [4, 2, 8, 6]

#output
20
```````
*/

const num = [4, 2, 8, 6];
let tampung = 0;

const tambah = () => {
  for (let plus = 0; plus < num.length; plus++) {
    // console.log(num[plus]);
    tampung += num[plus];
  }
  console.log(tampung);
}

tambah();

//============ Soal 5 ==================//
/*
Diberikan 2 buah array yang berisi angka.Array tersebut memiliki panjang yang sama sama. Buatlah sebuah fungsi yang membandingkan setiap elemen dari ke dua array tersebut.
Fungsi akan mengembalikan score hasil perbandingan ke dua array terbut.

```bash
var numA = [2, 3, 5]
var numB = [4, 1, 6]

#output
[1, 2]
```
*/

let numA = [2, 3, 5];
let numB = [4, 1, 6];


const array = () => {
  let tempArray = [];
  let hasilA = 0;
  let hasilB = 0;
  for (let i = 0; i < numA.length; i++) {
    //  console.log (numA[i]);
    //  console.log (numB[i]);
    if (numB[i] < numA[i]) {
      hasilA += 1;
    } else {
      hasilB += 1;
    }
  }
  tempArray.push(hasilA, hasilB);
  console.log(tempArray);
}


array();


//============ Soal 6 ==================//
/*
Diberikan sebuah array yang berisi 5 buah bilangan bulat positif. Cari nilai minimum dan maksimum yang dapat dihitung dengan menjumlahkan tepat empat elemen dari lima elemen array tersebut.

```bash
var num = [2, 3, 5, 1, 4]

#output
[10,14]
```
*/

let minHight = [2, 3, 5, 1, 4];

const kolom = () => {
  let arrayHasil = [];
  let hasilH = 0;
  let hasilL = 0;
  for (let i = 0; i < minHight.length; i++) {
    // console.log (minHight[i]);
    if (minHight[i] > 1) {
      hasilH += minHight[i];
    }
    if (minHight[i] < 5) {
      hasilL += minHight[i];
    }
  }
  arrayHasil.push(hasilL);
  arrayHasil.push(hasilH);
  console.log(arrayHasil);
}

kolom();


//============ Soal 7 ==================//
/*
Anda diberi daftar bilangan bulat n-1 dan bilangan bulat ini berada dalam kisaran 1 hingga n. Tidak ada duplikat dalam daftar hanya saja angkanya akan di acak. Salah satu bilangan bulat tidak ada dalam daftar. Buatlah sebuah fungsi yang akan mengembalikan angka yang hilang tersebut.

```bash
var num = [1, 3, 4, 2, 7, 6]

#output
5
```
*/

const missNumbr = [1, 3, 4, 2, 7, 6];

const missing = () => {
  // let maxArray = Math.max.apply(Math, missNumbr);
  // console.log (maxArray);
  let temp = [];

  for (let i = 1; i < missNumbr.length; i++) 
    // console.log (i);
    if (missNumbr.indexOf(i) < 0) {
      temp.push (i);
    }
  }
  console.log (temp);
} 


missing();