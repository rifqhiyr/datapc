/*
Di berikan sebuah angka !
1. Kalau angkanya ganjil tampilkan di layar "ganjil"
2. Kalau angkanya genap dan berada di antara 2 dan 5 tampilkan "OK"
3. Kalau angkanya genap dan berada di antara 6 dan 20 tampilkan "Tchuss"
4. Kalau angkanya genap dan berada lebih besar dari 20 tampilkan "Nihao"

contoh input dan output
3 --> ganjil
4 --> OK
17 --> ganjil
12 --> Tchuss
21 --> ganjil
22 --> nihao
*/

function isNumber(number) {
  if (number % 2 !== 0) {
    return `${number} is Odd`;
  } else {
    if (number >= 2 && number <= 5) {
      return `${number} is Even and OK`;
    } else if (number >= 6 && number <= 20) {
      return `${number} is Even and Tchuss`;
    } else if (number > 20) {
      return `${number} is Even and Nihao`;
    }
  }
}
console.log(isNumber(22));




/*
Karoke diskon membersip
1. Jika member tipenya bronze tampilkan "Biaya yang harus kamu bayar adalah 300rb"
2. Jika member tipenya silver tampilkan "Biaya yang harus kamu bayar adalah 400rb"
3. Jika member tipenya gold tampilkan "Biaya yang harus kamu bayar adalah 500rb"
4. Jika bukan member bernyanyi 1 jam tampilkan "Biaya yang harus kamu bayar adalah 50rb"
5. Jika bukan member bernyanyi 2 jam tampilkan "Biaya yang harus kamu bayar adalah 100rb"
6. Jika bukan member bernyanyi 3 jam tampilkan "Biaya yang harus kamu bayar adalah 150rb"
*/

function priceKaraoke(member, rent) {
  if (member === "bronze") {
    return `People is ${member} membership.`;
  } else if (member === "silver") {
    return `People is ${member} membership.`;
  } else if (member === "gold") {
    return `People is ${member} membership.`;
  } else {
    if (member === "notmember" && rent === 1) {
      return `People have no membership and must pay 50.000`;
    } else if (member === "notmember" && rent === 2) {
      return `People have no membership and must pay 100.000`;
    } else if (member === "notmember" && rent === 3) {
      return `People have no membership and must pay 150.000`;
    }
  }
}
console.log(priceKaraoke("notmember", 1));
